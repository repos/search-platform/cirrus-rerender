import logging
from pathlib import Path
from typing import Callable
from unittest.mock import Mock

import pytest
import requests
import requests_mock
import yaml
from pytest_mock import MockerFixture
from requests import Session

# we have to use an http scheme even for testing otherwise uri param are not handled
# https://github.com/jamielennox/requests-mock/issues/221
TEST_SCHEME = "https://"

logging.basicConfig(level=logging.WARN)


TEST_AUTH_TOKEN = "example-token"


@pytest.fixture
def request_adapter() -> requests_mock.Adapter:
    return requests_mock.Adapter()


@pytest.fixture
def test_session(request_adapter: requests_mock.Adapter) -> Session:
    session = requests.session()
    session.mount(TEST_SCHEME, request_adapter)
    return session


@pytest.fixture
def mw_client_auth_locator(tmp_path: Path) -> str:
    yaml_path = tmp_path / "secret.yaml"
    with yaml_path.open("wt") as f:
        yaml.dump({"path": {"to": {"secret": TEST_AUTH_TOKEN}}}, f)
    return f"{yaml_path}:path/to/secret"


@pytest.fixture
def mock_subprocess_run(mocker: MockerFixture) -> Callable[[str], Mock]:
    def fn(result: str, returncode: int = 0) -> Mock:
        mock_result = mocker.MagicMock()
        mock_result.returncode = returncode
        mock_result.stdout = result.encode("utf8")
        mock = mocker.patch("subprocess.run")
        mock.return_value = mock_result
        return mock

    return fn
