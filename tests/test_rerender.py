import json
from pathlib import Path
from typing import Any, Dict, List

from requests import Session
from requests_mock import Adapter

from cirrus_toolbox.rerender import arg_parser, main
from conftest import TEST_SCHEME

TEST_MW_ENDPOINT = f"{TEST_SCHEME}mw.unittest.local/w/api.php"
TEST_NOC_ENDPOINT = f"{TEST_SCHEME}noc.unittest.local/wiki.php"
TEST_EVENTGATE_ENDPOINT = f"{TEST_SCHEME}eventgate.unittest.local"
TEST_UA = "unittest UA"
TEST_WIKI = "unittestwiki"
TEST_WIKI_HOSTNAME = "www.unittestwiki.local"
TEST_WIKI_ENDPOINT = f"https://{TEST_WIKI_HOSTNAME}"

TEST_WIKI_CIRRUS_NSMAP = {
    "CirrusSearchConcreteNamespaceMap": {0: f"{TEST_WIKI}_content"},
    "CirrusSearchConcreteReplicaGroup": "my_replica_group",
}

SHARED_CLI_ARGS = [
    "--mw-endpoint",
    TEST_MW_ENDPOINT,
    "--noc-endpoint",
    TEST_NOC_ENDPOINT,
    "--eventgate",
    TEST_EVENTGATE_ENDPOINT,
    "--batch-size",
    "2",
    "--user-agent",
    TEST_UA,
    "--reason",
    TEST_UA,
]


def register_noc_mock(request_adapter: Adapter) -> None:
    request_adapter.register_uri(
        "GET",
        TEST_NOC_ENDPOINT,
        json={
            "wgCanonicalServer": TEST_WIKI_ENDPOINT,
            "wmgPrivateWiki": False,
        },
        request_headers={"user-agent": TEST_UA},
    )


def register_mwapi_mock(
    request_adapter: Adapter, mw_api_responses: List[Dict[str, Any]]
) -> None:
    request_adapter.register_uri(
        "GET",
        TEST_MW_ENDPOINT + "/w/api.php",
        response_list=mw_api_responses,
        request_headers={"user-agent": TEST_UA, "host": TEST_WIKI_HOSTNAME},
    )


def register_eventgate_mock(request_adapter: Adapter) -> None:
    request_adapter.register_uri(
        "POST", TEST_EVENTGATE_ENDPOINT, request_headers={"user-agent": TEST_UA}
    )


def test_from_allpages(
    test_session: Session, request_adapter: Adapter, mw_client_auth_locator: str
) -> None:
    parser = arg_parser()
    cli_args = SHARED_CLI_ARGS + [
        "--mediawiki-authorization-locator",
        mw_client_auth_locator,
        "allpages",
        "--wiki",
        TEST_WIKI,
        "--namespace",
        "0",
        "--from-title",
        "title1",
        "--to-title",
        "title2",
    ]
    register_noc_mock(request_adapter)
    register_mwapi_mock(
        request_adapter,
        [
            {"json": TEST_WIKI_CIRRUS_NSMAP},
            {
                "json": {
                    "batchcomplete": True,
                    "query": {"allpages": [{"pageid": 1, "title": "Title1", "ns": 0}]},
                },
            },
        ],
    )
    register_eventgate_mock(request_adapter)

    args = parser.parse_args(cli_args)
    retcode = main(args, test_session)
    assert retcode == 0

    # 1 noc, 1 mw-config, 1 for allpages, 1 for eventgate
    assert len(request_adapter.request_history) == 4
    event = request_adapter.request_history[3].json()[0]
    assert event["cirrussearch_cluster_group"] == "my_replica_group"
    assert event["cirrussearch_index_name"] == f"{TEST_WIKI}_content"
    assert not event["is_redirect"]
    assert event["namespace_id"] == 0
    assert event["page_id"] == 1
    assert event["page_title"] == "Title1"
    assert event["reason"] == TEST_UA
    assert event["wiki_id"] == TEST_WIKI


def test_from_ndjson(
    test_session: Session,
    request_adapter: Adapter,
    mw_client_auth_locator: str,
    tmp_path: Path,
) -> None:
    test_path = tmp_path / "test.json"
    with test_path.open("wt") as f:
        f.write(
            json.dumps(
                {
                    "wiki": TEST_WIKI,
                    "page_id": 1,
                    "namespace": 0,
                    "title": "Title1",
                }
            )
            + "\n"
        )

    parser = arg_parser()
    cli_args = SHARED_CLI_ARGS + [
        "--mediawiki-authorization-locator",
        mw_client_auth_locator,
        "ndjson",
        "--path",
        str(test_path),
    ]

    register_noc_mock(request_adapter)
    register_mwapi_mock(
        request_adapter,
        [
            {"json": TEST_WIKI_CIRRUS_NSMAP},
        ],
    )
    register_eventgate_mock(request_adapter)

    args = parser.parse_args(cli_args)
    retcode = main(args, test_session)
    assert retcode == 0

    assert len(request_adapter.request_history) == 3
    event = request_adapter.request_history[2].json()[0]
    assert event["cirrussearch_cluster_group"] == "my_replica_group"
    assert event["cirrussearch_index_name"] == f"{TEST_WIKI}_content"
    assert not event["is_redirect"]
    assert event["namespace_id"] == 0
    assert event["page_id"] == 1
    assert event["page_title"] == "Title1"
    assert event["reason"] == TEST_UA
    assert event["wiki_id"] == TEST_WIKI
