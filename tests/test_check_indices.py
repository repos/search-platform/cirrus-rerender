import re
from collections.abc import Callable
from unittest.mock import Mock

from pytest_mock import MockerFixture

from cirrus_toolbox.check_indices import (
    CirrusExpectedIndicesGenerator,
    ElasticsearchState,
    build_config,
)
from cirrus_toolbox.mw_api_client import MWClient
from cirrus_toolbox.noc_client import NOCClient


def test_build_config_and_expected_indices(
    mock_subprocess_run: Callable[[str], Mock], mocker: MockerFixture
) -> None:
    mock_subprocess_run("test1wiki\ntest2wiki")

    def mw_client_factory(hostname: str) -> MWClient:
        mwclient = mocker.Mock(MWClient)
        wiki_id = re.sub(r"^www\.(\w+)\.unittest\.local$", r"\1", hostname)

        mwclient.expected_indices.return_value = {
            "dbname": hostname,
            "clusters": {
                "eqiad": {
                    "aliases": [
                        f"{wiki_id}_content",
                        f"{wiki_id}_general",
                    ],
                    "group": "chi",
                },
                "codfw": {
                    "aliases": [
                        f"{wiki_id}_content",
                        f"{wiki_id}_general",
                    ],
                    "group": "chi",
                },
                "cloudelastic": {
                    "aliases": [
                        f"{wiki_id}_content",
                        f"{wiki_id}_general",
                    ],
                    "group": "chi",
                },
            },
        }
        return mwclient

    noc_client = mocker.Mock(NOCClient)

    def get_hostname_by_wiki(wiki_id: str) -> str:
        return f"www.{wiki_id}.unittest.local"

    noc_client.get_hostname_by_wiki_id.side_effect = get_hostname_by_wiki
    config = build_config(noc_client, mw_client_factory)
    config_map = {c.key: c for c in config}
    assert len(config_map) == 3 * 3  # 3 replicas * 3 groups
    prod_chi_eqiad = config_map[("eqiad", "chi")]
    cirrus_expected_indices_acceptor = prod_chi_eqiad.accept[0]
    assert isinstance(cirrus_expected_indices_acceptor, CirrusExpectedIndicesGenerator)
    cluster_state = ElasticsearchState(
        "production-eqiad-chi",
        "eqiad",
        "chi",
        indices={
            "test1wiki_content_first",
            "test1wiki_general_123",
            "test2wiki_content_234",
            "test2wiki_general_463",
        },
        aliases={
            "test1wiki_content": "test1wiki_content_first",
            "test1wiki_general": "test1wiki_content_123",
            "test2wiki_content": "test2wiki_content_234",
            "test2wiki_general": "test2wiki_content_463",
        },
    )

    actual_accepted = [
        a for a in cirrus_expected_indices_acceptor.accept(cluster_state)
    ]
    expected_accepted = [
        "mw_cirrus_metastore",
        "test1wiki_content_first",
        "test1wiki_content_123",
        "test2wiki_content_234",
        "test2wiki_content_463",
    ]
    assert actual_accepted == expected_accepted

    cluster_state = ElasticsearchState(
        "production-eqiad-codfw",
        "codfw",
        "chi",
        indices={
            "mw_cirrus_metastore_first",
        },
        aliases={
            "mw_cirrus_metastore": "mw_cirrus_metastore_first",
        },
    )

    actual_accepted = [
        a for a in cirrus_expected_indices_acceptor.accept(cluster_state)
    ]
    expected_accepted = [
        "mw_cirrus_metastore_first",
        "test1wiki_content",
        "test1wiki_general",
        "test2wiki_content",
        "test2wiki_general",
    ]
    assert actual_accepted == expected_accepted
    # TODO: add more cases
