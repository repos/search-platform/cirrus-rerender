import urllib.parse
from pathlib import Path
from typing import Any, Dict, List

import pytest
import requests
import requests_mock
import yaml

from cirrus_toolbox.mw_api_client import (
    CirrusIndexAndReplicaGroup,
    MWApiError,
    MWClient,
    Page,
)
from conftest import TEST_SCHEME

TEST_ENDPOINT = f"{TEST_SCHEME}mw-api.unittest.local:1234"
TEST_WIKI_DOMAIN = "mywiki.unittest.local"
REQUEST_HEADERS = {"host": TEST_WIKI_DOMAIN}


def test_get_cirrus_index_ns_map(
    test_session: requests.Session, request_adapter: requests_mock.Adapter
) -> None:
    config = {
        "CirrusSearchConcreteNamespaceMap": {
            "0": "mywiki_content",
            "1": "mywiki_general",
            "2": "mywiki_general",
            "3": "mywiki_general",
        },
        "CirrusSearchConcreteReplicaGroup": "my_group",
    }
    expected_map = {
        0: CirrusIndexAndReplicaGroup("mywiki_content", "my_group"),
        1: CirrusIndexAndReplicaGroup("mywiki_general", "my_group"),
        2: CirrusIndexAndReplicaGroup("mywiki_general", "my_group"),
        3: CirrusIndexAndReplicaGroup("mywiki_general", "my_group"),
    }
    params = {
        "action": "cirrus-config-dump",
        "prop": "replicAgroup|namespacemap",
        "format": "json",
        "formatversion": "2",
    }

    request_adapter.register_uri(
        "GET",
        TEST_ENDPOINT + "/w/api.php?" + urllib.parse.urlencode(params),
        json=config,
        complete_qs=True,
        request_headers=REQUEST_HEADERS,
    )
    client = MWClient(test_session, TEST_ENDPOINT, TEST_WIKI_DOMAIN, None)
    assert client.get_cirrus_index_ns_map() == expected_map


def _gen_pages(from_id: int, to_id: int, ns: int) -> List[Dict[str, Any]]:
    return [
        {"pageid": i, "title": f"Title{i}", "ns": ns} for i in range(from_id, to_id)
    ]


def test_all_pages(
    test_session: requests.Session, request_adapter: requests_mock.Adapter
) -> None:
    from_page = "Title0"
    to_page = "Title520"
    params = {
        "action": "query",
        "list": "allpages",
        "apfilterredir": "nonredirects",
        "aplimit": "500",
        "apnamespace": "0",
        "apfrom": from_page,
        "apto": to_page,
    }
    format_params = {
        "format": "json",
        "formatversion": "2",
    }
    consecutive_responses = [
        {
            "json": {
                "batchcomplete": True,
                "continue": {"apcontinue": "Title500", "continue": "-||"},
                "query": {"allpages": _gen_pages(0, 500, 0)},
            },
        },
        {
            "json": {
                "batchcomplete": True,
                "query": {"allpages": _gen_pages(500, 520, 0)},
            }
        },
    ]
    request_adapter.register_uri(
        "GET",
        TEST_ENDPOINT + "/w/api.php?" + urllib.parse.urlencode(params),
        response_list=consecutive_responses,
        request_headers=REQUEST_HEADERS,
    )
    client = MWClient(test_session, TEST_ENDPOINT, TEST_WIKI_DOMAIN, None)
    pages = list(client.all_pages(0, from_page, to_page))
    expected_pages = [Page(i, 0, f"Title{i}") for i in range(0, 520)]
    assert pages == expected_pages

    continue_params = {"apcontinue": "Title500", "continue": "-||"}
    hist = request_adapter.request_history
    assert len(hist) == 2
    # request_mock is case-insensitive by default and for some reason I could not enable case-sensitivity...
    assert hist[0].query == urllib.parse.urlencode({**params, **format_params}).lower()
    assert (
        hist[1].query
        == urllib.parse.urlencode(
            {**params, **continue_params, **format_params}
        ).lower()
    )


def test_allpages_with_invalid_body(
    test_session: requests.Session, request_adapter: requests_mock.Adapter
) -> None:
    request_adapter.register_uri("GET", TEST_ENDPOINT + "/w/api.php", json={})
    client = MWClient(test_session, TEST_ENDPOINT, TEST_WIKI_DOMAIN, None)
    with pytest.raises(RuntimeError):
        list(client.all_pages(None))


def test_raises_api_error(
    test_session: requests.Session, request_adapter: requests_mock.Adapter
) -> None:
    request_adapter.register_uri(
        "GET", TEST_ENDPOINT + "/w/api.php", json={"error": {}}
    )
    client = MWClient(test_session, TEST_ENDPOINT, TEST_WIKI_DOMAIN, None)
    with pytest.raises(MWApiError):
        client.get_cirrus_index_ns_map()


def test_auth_locator(tmp_path: Path) -> None:
    yaml_path = tmp_path / "test_auth_locator_secret.yaml"
    with yaml_path.open("wt") as f:
        yaml.dump({"path": {"to": {"secret": "my_secret"}}}, f)
    locator = f"{yaml_path}:path/to/secret"
    token = MWClient._source_auth_header(locator)
    assert token == "NetworkSession my_secret"


def test_expected_indices(
    test_session: requests.Session, request_adapter: requests_mock.Adapter
) -> None:
    actual_expected_indices = {
        "CirrusSearchExpectedIndices": {
            "dbname": "testwiki",
            "clusters": {
                "dc1": {
                    "aliases": [
                        "testwiki_content",
                        "testwiki_general",
                        "testwiki_archive",
                        "testwiki_titlesuggest",
                    ],
                    "shard_count": {
                        "testwiki_content": 1,
                        "testwiki_general": 1,
                        "testwiki_archive": 1,
                        "testwiki_titlesuggest": 1,
                    },
                    "group": "group1",
                },
                "dc2": {
                    "aliases": [
                        "testwiki_content",
                        "testwiki_general",
                        "testwiki_archive",
                        "testwiki_titlesuggest",
                    ],
                    "shard_count": {
                        "testwiki_content": 1,
                        "testwiki_general": 1,
                        "testwiki_archive": 1,
                        "testwiki_titlesuggest": 1,
                    },
                    "group": "group1",
                },
            },
        }
    }

    expected_result = {
        "dbname": "testwiki",
        "clusters": {
            "dc1": {
                "aliases": [
                    "testwiki_content",
                    "testwiki_general",
                    "testwiki_archive",
                    "testwiki_titlesuggest",
                ],
                "shard_count": {
                    "testwiki_content": 1,
                    "testwiki_general": 1,
                    "testwiki_archive": 1,
                    "testwiki_titlesuggest": 1,
                },
                "group": "group1",
            },
            "dc2": {
                "aliases": [
                    "testwiki_content",
                    "testwiki_general",
                    "testwiki_archive",
                    "testwiki_titlesuggest",
                ],
                "shard_count": {
                    "testwiki_content": 1,
                    "testwiki_general": 1,
                    "testwiki_archive": 1,
                    "testwiki_titlesuggest": 1,
                },
                "group": "group1",
            },
        },
    }

    params = {
        "action": "cirrus-config-dump",
        "prop": "expectedindices",
        "format": "json",
        "formatversion": "2",
    }

    request_adapter.register_uri(
        "GET",
        TEST_ENDPOINT + "/w/api.php?" + urllib.parse.urlencode(params),
        json=actual_expected_indices,
        complete_qs=True,
        request_headers=REQUEST_HEADERS,
    )
    client = MWClient(test_session, TEST_ENDPOINT, TEST_WIKI_DOMAIN, None)
    assert client.expected_indices() == expected_result
