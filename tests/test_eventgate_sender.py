import math
import re
import time
import uuid
from datetime import datetime, timezone

from requests import Session
from requests_mock import Adapter

from cirrus_toolbox.eventgate_sender import EventGateSender, ratelimit
from cirrus_toolbox.mw_api_client import CirrusIndexAndReplicaGroup, Page
from conftest import TEST_SCHEME

TEST_EVENT_GATE_ENDPOINT = f"{TEST_SCHEME}eventgate.local/v1/events"
TEST_WIKI_ID = "unittestwiki"
TEST_WIKI_HOSTNAME = "wiki.unittest.local"
TEST_REPLICA_GROUP = "my_replica_group"
TEST_NS_MAP = {
    0: CirrusIndexAndReplicaGroup(f"{TEST_WIKI_ID}_content", TEST_REPLICA_GROUP),
    1: CirrusIndexAndReplicaGroup(f"{TEST_WIKI_ID}_general", TEST_REPLICA_GROUP),
}
NOW = datetime.now(tz=timezone.utc)
DT = re.sub(r"\+00:00$", "Z", NOW.replace(microsecond=0).isoformat())
ID = str(uuid.uuid4())


def clock() -> datetime:
    return NOW


def id_gen() -> str:
    return ID


def test_send_event(test_session: Session, request_adapter: Adapter) -> None:
    client = EventGateSender(
        test_session, TEST_EVENT_GATE_ENDPOINT, 2, 1000, clock, id_gen
    )
    request_adapter.register_uri("POST", TEST_EVENT_GATE_ENDPOINT)
    events = [{"event": i} for i in range(3)]
    client.send(events)
    assert len(request_adapter.request_history) == 2
    assert request_adapter.request_history[0].json() == events[:2]
    assert request_adapter.request_history[1].json() == events[2:]


def test_send_public_rerender_events(
    test_session: Session, request_adapter: Adapter
) -> None:
    client = EventGateSender(
        test_session, TEST_EVENT_GATE_ENDPOINT, 2, 1000, clock, id_gen
    )
    pages = [Page(1, 0, "Title1"), Page(2, 1, "Talk:Title1")]
    request_adapter.register_uri("POST", TEST_EVENT_GATE_ENDPOINT)
    client.send_page_rerenders(
        TEST_WIKI_HOSTNAME, TEST_WIKI_ID, False, "test reason", pages, TEST_NS_MAP
    )
    assert len(request_adapter.request_history) == 1
    assert request_adapter.request_history[0].json() == [
        {
            "$schema": "/mediawiki/cirrussearch/page_rerender/1.0.0",
            "meta": {
                "domain": TEST_WIKI_HOSTNAME,
                "id": ID,
                "request_id": ID,
                "stream": "mediawiki.cirrussearch.page_rerender.v1",
            },
            "dt": DT,
            "cirrussearch_cluster_group": TEST_REPLICA_GROUP,
            "cirrussearch_index_name": "unittestwiki_content",
            "is_redirect": False,
            "namespace_id": 0,
            "page_id": 1,
            "page_title": "Title1",
            "reason": "test reason",
            "wiki_id": TEST_WIKI_ID,
        },
        {
            "$schema": "/mediawiki/cirrussearch/page_rerender/1.0.0",
            "meta": {
                "domain": TEST_WIKI_HOSTNAME,
                "id": ID,
                "request_id": ID,
                "stream": "mediawiki.cirrussearch.page_rerender.v1",
            },
            "dt": DT,
            "cirrussearch_cluster_group": TEST_REPLICA_GROUP,
            "cirrussearch_index_name": "unittestwiki_general",
            "is_redirect": False,
            "namespace_id": 1,
            "page_id": 2,
            "page_title": "Talk:Title1",
            "reason": "test reason",
            "wiki_id": TEST_WIKI_ID,
        },
    ]


def test_send_private_rerender_events(
    test_session: Session, request_adapter: Adapter
) -> None:
    client = EventGateSender(
        test_session, TEST_EVENT_GATE_ENDPOINT, 2, 1000, clock, id_gen
    )
    pages = [Page(1, 0, "Title1")]
    request_adapter.register_uri("POST", TEST_EVENT_GATE_ENDPOINT)
    client.send_page_rerenders(
        TEST_WIKI_HOSTNAME, TEST_WIKI_ID, True, "test reason", pages, TEST_NS_MAP
    )
    assert len(request_adapter.request_history) == 1
    assert request_adapter.request_history[0].json() == [
        {
            "$schema": "/mediawiki/cirrussearch/page_rerender/1.0.0",
            "meta": {
                "domain": TEST_WIKI_HOSTNAME,
                "id": ID,
                "request_id": ID,
                "stream": "mediawiki.cirrussearch.page_rerender.private.v1",
            },
            "dt": DT,
            "cirrussearch_cluster_group": TEST_REPLICA_GROUP,
            "cirrussearch_index_name": "unittestwiki_content",
            "is_redirect": False,
            "namespace_id": 0,
            "page_id": 1,
            "page_title": "Title1",
            "reason": "test reason",
            "wiki_id": TEST_WIKI_ID,
        }
    ]


def test_ratelimit():
    seq = list(range(5))
    target_duration = 1.0
    sec_per_item = target_duration / len(seq)

    start = time.monotonic()
    list(ratelimit(seq, sec_per_item))
    end = time.monotonic()
    actual_duration = end - start
    assert math.isclose(actual_duration, target_duration, rel_tol=0.1, abs_tol=0.1)
