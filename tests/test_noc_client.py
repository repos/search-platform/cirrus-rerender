from typing import Any, Dict, Iterable, Mapping

import pytest
from requests import Session
from requests_mock import Adapter

from cirrus_toolbox.noc_client import NOCClient
from conftest import TEST_SCHEME

NOC_TEST_ENDPOINT = f"{TEST_SCHEME}noc.unittest.local/wiki.php"


def provide_test_get_wiki_hostname() -> Iterable[Dict[str, Any]]:
    yield {
        "wiki_param": "mywiki",
        "response": {"wgCanonicalServer": "https://mywiki.unittest.local"},
        "expected_hostname": "mywiki.unittest.local",
    }
    yield {
        "wiki_param": "mywiki2",
        "response": {"wgCanonicalServer": "http://mywiki2.unittest.local"},
        "expected_hostname": "mywiki2.unittest.local",
    }
    yield {
        "wiki_param": "enwiki",
        "response": {"wgCanonicalServer": "https://en.wikipedia.org"},
        "expected_hostname": "en.wikipedia.org",
    }


@pytest.fixture
def noc_client(test_session: Session) -> NOCClient:
    return NOCClient(test_session, NOC_TEST_ENDPOINT)


def register_wiki(
    request_adapter: Adapter, wiki: str, response: Mapping[str, Any]
) -> None:
    request_adapter.register_uri(
        "GET",
        f"{NOC_TEST_ENDPOINT}?wiki={wiki}&format=json",
        json=response,
    )


@pytest.mark.parametrize("test_input", provide_test_get_wiki_hostname())
def test_get_wiki_hostname(
    test_input: Dict[str, Any], request_adapter: Adapter, noc_client: NOCClient
) -> None:
    wiki = test_input["wiki_param"]
    register_wiki(request_adapter, wiki, test_input["response"])
    assert noc_client.get_hostname_by_wiki_id(wiki) == test_input["expected_hostname"]


def test_get_is_private(request_adapter: Adapter, noc_client: NOCClient) -> None:
    register_wiki(request_adapter, "testwiki", {"wmgPrivateWiki": False})
    register_wiki(request_adapter, "officewiki", {"wmgPrivateWiki": True})

    assert not noc_client.get_is_private("testwiki")
    assert noc_client.get_is_private("officewiki")
