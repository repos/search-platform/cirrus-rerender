# Cirrus WMF Toolbox

This project is a set of tools useful to manage the WMF CirrusSearch installation.

## Tools

### Force rerender
The tool scans the MW api to ship cirrus rerender events forcing SUP to re-parse
these pages and update the elasticsearch index.
It is not designed to work on large scale wikis such as enwiki.

#### Invocation
Rerender all testwiki pages:

    $ python -m cirrus_toolbox.rerender allpages --wiki testwiki

Rerender all `NS_MAIN` pages from Title1 to Title100:

    $ python -m cirrus_toolbox.rerender allpages --wiki testwiki --namespace 0 --from-title Title1 --to-title Title100

Rerender pages from an ndjson file:

    $ python -m cirrus_toolbox.rerender ndjson --path example.ndjson

example.ndjson:

    {"wiki": "testwiki", "page_id": 1, "namespace": 0, "title": "Example1"}
    {"wiki": "otherwiki", "page_id": 42, "namespace": 12, "title": "Example2"}

### Check indices

    $ python -m cirrus_toolbox.check_indices --help

### Monitor recent change

    $ python -m cirrus_toolbox.check_rc --help

### Clean up `weighted_tags`

    $ python -m cirrus_toolbox.cleanup_weighted_tags --help

### Compare clusters

    $ python -m cirrus_toolbox.compare-clusters --help

### Push cross cluster settings

    $ python -m cirrus_toolbox.push_cross_cluster_conf --help

## Build and Deploy

### Lints and Tests

Uses python standard tox, configured via setup.cfg, for managing test environments
and commands.

### black

black is used to format python code before commiting. It is checked as part of the
CI pipeline by running bare `tox`.  To reformat the repository:

    tox -e black-fix

### isort

isort is used to sort imports before commiting. It is checked as part of the CI
pipeline be running bare `tox`. To reformat the repository:

    tox -e isort-fix

## Packaging

A python wheel package is build by the data platform engineering teams
`python_lib_repo` CI pipeline. Select `trigger_release` from the `Pipelines`
tab in gitlab for the top commit to the main branch to release the next
version and upload it to the project pypi index.

### Deployment

While more can be done, for now the module has no external requirements and
can be installed directly to a virtualenv without accessing the internet
for dependencies.

    $ mkdir ~/cirrus-toolbox.$(date '+%Y%m%d')
    $ cd !$
    $ python3 -m venv --system-site-packages venv
    $ venv/bin/pip install cirrus-toolbox --index-url https://gitlab.wikimedia.org/api/v4/projects/2364/packages/pypi/simple

NOTE: `cleanup_weighted_tags` requires the elasticsearch client:

    $ venv/bin/pip install cirrus-toolbox[elasticsearch] --index-url https://gitlab.wikimedia.org/api/v4/projects/2364/packages/pypi/simple

## Example invocations

