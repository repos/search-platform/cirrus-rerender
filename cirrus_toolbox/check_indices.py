"""
Reconcile expected indices against live cluster state

Reconciles the state of multiple elasticsearch clusters against the expected
state of multiple wikis. The reconciliation process is accept based. Some
number of acceptors can be configured per-cluster. Each implementation receives
information about the state of a cluster and is expected to yield the set of
indices it expects to exist on that cluster. The output is then the set of
indices that exist but should not, and the set of indices that should exist but
do not. Some attempts are made to explain why a specific index should not
exist.
"""

from __future__ import annotations

import itertools
import json
import logging
import re
import subprocess
import sys
from argparse import ArgumentParser
from collections import Counter, defaultdict
from collections.abc import Callable
from dataclasses import dataclass
from enum import Enum
from functools import cached_property, total_ordering
from typing import (
    Generator,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    NamedTuple,
    Optional,
    Pattern,
    Sequence,
    Set,
    Tuple,
)

from requests import Session

from cirrus_toolbox.mw_api_client import (
    DEFAULT_AUTH_TOKEN_LOCATOR,
    DEFAULT_MW_ENDPOINT,
    MWClient,
)
from cirrus_toolbox.noc_client import DEFAULT_NOC_ENDPOINT, NOCClient

log = logging.getLogger(__name__)


DEFAULT_UA = "cirrus_toolbox.check_indices/WMF"


@dataclass
class ElasticsearchState:
    cluster_name: str
    replica: str
    group: str
    indices: Set[str]
    aliases: Mapping[str, str]

    @cached_property
    def key(self) -> Tuple[str, str]:
        return self.replica, self.group


# Configuration of the checker for one elasticsearch cluster.
@dataclass
@total_ordering
class ClusterToCheck:
    cluster_name: str
    replica: str
    group: str
    base_url: str
    accept: Sequence[IndexAcceptor]

    @cached_property
    def key(self) -> Tuple[str, str]:
        return self.replica, self.group

    def __eq__(self, other: object) -> bool:
        if isinstance(other, ClusterToCheck):
            return self.key.__eq__(other.key)
        return False

    def __lt__(self, other: ClusterToCheck) -> bool:
        return self.key.__lt__(other.key)


@dataclass
class CirrusCluster:
    """Expected state of a cluster for a single wiki"""

    replica: str
    group: str
    aliases: Sequence[str]

    @cached_property
    def key(self) -> Tuple[str, str]:
        return self.replica, self.group


@dataclass
class WikiState:
    """Expected state of a single wiki"""

    dbname: str
    clusters: Sequence[CirrusCluster]


class ProblemKind(Enum):
    MISSING = 1  # Should exist but does not
    EXTRA = 2  # Should not exist but does
    OTHER = 3  # bad configuration?


# An index that doesn't match the expected state
Problem = NamedTuple(
    "Problem",
    [
        ("cluster", ClusterToCheck),
        ("index", str),
        ("kind", ProblemKind),
        ("reason", str),
    ],
)


class IndexAcceptor:
    def accept(self, cluster_state: ElasticsearchState) -> Generator[str, None, None]:
        raise NotImplementedError

    def explain(
        self, problem: Problem, es_state: Mapping[Tuple[str, str], ElasticsearchState]
    ) -> Optional[str]:
        return None


def make_elasticsearch_state(
    session: Session, config: ClusterToCheck
) -> ElasticsearchState:
    base_url = config.base_url
    res = session.get(base_url)
    res.raise_for_status()
    health = res.json()
    if health["cluster_name"] != config.cluster_name:
        raise Exception(
            "Cluster at {} expected to be {} but found {}".format(
                base_url, config.cluster_name, health["cluster_name"]
            )
        )

    res = session.get(
        base_url + "/_cat/indices",
        headers={
            "Accept": "application/json",
        },
    )
    res.raise_for_status()
    indices = {index["index"] for index in res.json() if index["status"] == "open"}

    res = session.get(
        base_url + "/_cat/aliases",
        headers={
            "Accept": "application/json",
        },
    )
    res.raise_for_status()
    aliases = {alias["alias"]: alias["index"] for alias in res.json()}

    return ElasticsearchState(
        health["cluster_name"], config.replica, config.group, indices, aliases
    )


def all_dbnames() -> Iterable[str]:
    output = subprocess.run(["expanddblist", "all"], check=True, capture_output=True)
    return output.stdout.decode("utf8").strip().split("\n")


def make_wiki_state(
    dbname: str, noc_client: NOCClient, mw_client_factory: Callable[[str], MWClient]
) -> WikiState:
    hostname = noc_client.get_hostname_by_wiki_id(dbname)
    mw_client = mw_client_factory(hostname)
    raw = mw_client.expected_indices()
    return WikiState(
        raw["dbname"],
        [
            CirrusCluster(replica, state["group"], state["aliases"])
            for replica, state in raw["clusters"].items()
        ],
    )


def validate_cluster(
    config: ClusterToCheck,
    cluster_state: ElasticsearchState,
) -> Generator[Problem, None, None]:
    accepted = [
        set(index_acceptor.accept(cluster_state)) for index_acceptor in config.accept
    ]
    unique = set().union(*accepted)

    if len(unique) != sum(len(x) for x in accepted):
        yield from report_duplicate_accepted(config, accepted)

    extra = cluster_state.indices.difference(unique)
    for index in extra:
        yield Problem(config, index, ProblemKind.EXTRA, "Index not expected on cluster")
    missing = unique.difference(cluster_state.indices)
    for index in missing:
        yield Problem(config, index, ProblemKind.MISSING, "Expected index was missing")


def try_to_explain(
    problem: Problem,
    es_state: Mapping[Tuple[str, str], ElasticsearchState],
) -> Optional[str]:
    for index_acceptor in problem.cluster.accept:
        explain = index_acceptor.explain(problem, es_state)
        if explain:
            return explain
    return None


def validate_clusters(
    session: Session, clusters: Generator[ClusterToCheck, None, None]
) -> Generator[Problem, None, None]:
    clusters_list = list(clusters)
    es_states = {
        cluster.key: make_elasticsearch_state(session, cluster)
        for cluster in clusters_list
    }
    for cluster in clusters_list:
        for problem in validate_cluster(cluster, es_states[cluster.key]):
            explain = try_to_explain(problem, es_states)
            if explain:
                yield Problem(problem.cluster, problem.index, problem.kind, explain)
            else:
                yield problem


def report_duplicate_accepted(
    config: ClusterToCheck, accepted: Iterable[Iterable[str]]
) -> Generator[Problem, None, None]:
    counts = Counter(item for sublist in accepted for item in sublist)
    for index, repeats in counts.items():
        if repeats > 1:
            msg = "Accepted by {} different acceptors".format(repeats)
            yield Problem(config, index, ProblemKind.OTHER, msg)


def marker_ts(marker: str) -> int:
    # Converts the final portion of a cirrussearch index name,
    # the 12345 from devwiki_content_12345, into it's integer
    # complement.
    if marker == "first":
        return 0
    return int(marker)


class CirrusExpectedIndicesGenerator(IndexAcceptor):
    @classmethod
    def from_wiki_states(
        cls, wiki_states: Sequence[WikiState]
    ) -> CirrusExpectedIndicesGenerator:
        clusters: MutableMapping[Tuple[str, str], List[str]] = defaultdict(list)
        for wiki_state in wiki_states:
            for cirrus_cluster in wiki_state.clusters:
                clusters[cirrus_cluster.key] += cirrus_cluster.aliases
        return cls(clusters)

    def __init__(self, clusters: Mapping[Tuple[str, str], Sequence[str]]):
        self.clusters = clusters

    def accept(self, cluster_state: ElasticsearchState) -> Generator[str, None, None]:
        # metastore should exist on all cirrussearch clusters
        try:
            yield cluster_state.aliases["mw_cirrus_metastore"]
        except KeyError:
            yield "mw_cirrus_metastore"

        key = (cluster_state.replica, cluster_state.group)
        is_cloud = cluster_state.cluster_name.startswith("cloudelastic-")
        for alias in self.clusters[key]:
            try:
                yield cluster_state.aliases[alias]
            except KeyError:
                is_titlesuggest = alias.endswith("_titlesuggest")
                # hax: cloudelastic could have titlesuggest, but we don't build
                # those. Ignore the problem for now.
                if not (is_cloud and is_titlesuggest):
                    yield alias

    INDEX_PAT = re.compile(r"^(\w+)_(\w+)_(first|\d+)$")

    def explain(
        self,
        problem: Problem,
        es_state: Mapping[Tuple[str, str], ElasticsearchState],
    ) -> Optional[str]:
        """Explain why this index wasn't accepted

        Only responds to extra indices that were not accepted but
        plausibly could have been.
        """
        if problem.kind != ProblemKind.EXTRA:
            return None
        match = self.INDEX_PAT.search(problem.index)
        if not match:
            return None
        wiki, index_type, marker = match.groups()
        expected_alias = "{}_{}".format(wiki, index_type)
        if expected_alias in self.clusters[problem.cluster.key]:
            # This type of index should exist, is there a different live index?
            cluster_state = es_state[problem.cluster.key]
            try:
                live_index = cluster_state.aliases[expected_alias]
            except KeyError:
                return "Index alias for {} expected but does not exist".format(
                    expected_alias
                )
            # Make a guess based on timestamps, if the index is older than
            # the live index it's very likely dead. If newer it could be
            # a live reindex or a failed reindex. We could try poking
            # _tasks api to find live reindexes?
            live_match = self.INDEX_PAT.search(live_index)
            if not live_match:
                return (
                    "Duplicate of live index {}."
                    "Live index doesnt have valid name format?"
                ).format(live_index)
            live_marker = live_match.group()[-1]
            try:
                if marker_ts(live_marker) > marker_ts(marker):
                    reason = "Reindex in progress?"
                else:
                    reason = "Failed reindex?"
            except TypeError:
                reason = "One of the index naming formats is unrecognized"
            return "Duplicate of live index " + live_index + ". " + reason

        # This kind of index was not expected here, is it supposed to be on a
        # a different cluster in the same replica?
        expected_clusters = [
            key
            for key, aliases in self.clusters.items()
            if key[0] == problem.cluster.replica and expected_alias in aliases
        ]
        if expected_clusters:
            return "Index on wrong cluster of replica, expected in " + ", ".join(
                es_state[key].cluster_name for key in expected_clusters
            )
        if any(expected_alias in aliases for aliases in self.clusters.values()):
            return (
                "Index not expected in this group."
                "Private index on non-private cluster?"
            )
        return "Looks like Cirrus, but did not expect to exist." "Deleted wiki?"


@dataclass
class IndexPatternAcceptor(IndexAcceptor):
    """Accept indices by regex match against index name"""

    re_pattern: Pattern

    def accept(self, cluster_state: ElasticsearchState) -> Generator[str, None, None]:
        for index_name in cluster_state.indices:
            if self.re_pattern.search(index_name):
                yield index_name


class GlentIndexAcceptor(IndexAcceptor):
    """Accept glent production indices

    At any given time glent should have two live indices, sometimes a
    third if an import is currently running. The two allowed live indices
    are identified via the index aliases. A third index is only accepted
    if it has a date newer than the other live indices.
    """

    def accept(self, cluster_state: ElasticsearchState) -> Generator[str, None, None]:
        # Expect two hardcoded alias names and accept the concrete
        # indices they point to.
        expected_aliases = {"glent_production", "glent_rollback"}
        found = {
            cluster_state.aliases[alias]
            for alias in expected_aliases
            if alias in cluster_state.aliases
        }
        yield from found


def build_config(
    noc_client: NOCClient, mw_client_factory: Callable[[str], MWClient]
) -> Generator[ClusterToCheck, None, None]:
    wiki_state = [
        make_wiki_state(db, noc_client, mw_client_factory) for db in all_dbnames()
    ]

    accept_all = [
        CirrusExpectedIndicesGenerator.from_wiki_states(wiki_state),
        # internal elasticsearch index
        IndexPatternAcceptor(re.compile(r"^\.tasks$")),
        # ltr plugin for elasticsearch
        IndexPatternAcceptor(re.compile(r"^\.ltrstore$")),
    ]
    # Indices that are not per-wiki are only found in chi
    accept_by_group = {
        "chi": [
            GlentIndexAcceptor(),
            IndexPatternAcceptor(re.compile(r"^apifeatureusage-\d\d\d\d\.\d\d\.\d\d$")),
            IndexPatternAcceptor(re.compile(r"^ttmserver(-test)?$")),
            IndexPatternAcceptor(re.compile(r"^phabricator$")),
        ]
    }

    ports = {"chi": 9243, "omega": 9443, "psi": 9643}
    for replica in ("eqiad", "codfw"):
        for group, port in ports.items():
            accept = accept_all + accept_by_group.get(group, [])
            name = "production-search-{}-{}".format(group, replica)
            if group == "chi":
                # chi was at one time the only group and still has
                # the old name not including the group.
                name = "production-search-{}".format(replica)
            yield ClusterToCheck(
                cluster_name=name,
                replica=replica,
                group=group,
                # TODO: stop hardcoding this connection info
                base_url="https://search.svc.{}.wmnet:{}".format(replica, ports[group]),
                accept=accept,
            )

    for group, port in ports.items():
        yield ClusterToCheck(
            cluster_name="cloudelastic-{}-eqiad".format(group),
            replica="cloudelastic",
            group=group,
            base_url="https://cloudelastic.wikimedia.org:" + str(port),
            # cloudelastic contains only cirrus indices, nothing secondary
            accept=accept_all,
        )


def sort_and_group(
    iterable: Iterable[Problem], key: Callable[[Problem], ClusterToCheck]
) -> Iterable[Tuple[ClusterToCheck, Iterable[Problem]]]:
    return itertools.groupby(sorted(iterable, key=key), key=key)


def report_problems(problems: Generator[Problem, None, None]) -> Sequence[Mapping]:
    get_cluster: Callable[[Problem], ClusterToCheck] = lambda p: p.cluster
    grouped = sort_and_group(problems, key=get_cluster)
    return [
        {
            "cluster_name": cluster.cluster_name,
            "url": cluster.base_url,
            "problems": [
                {
                    "index": p.index,
                    "reason": p.reason,
                }
                for p in problems
            ],
        }
        for cluster, problems in grouped
    ]


def main(
    mw_endpoint: str,
    mediawiki_authorization_locator: str,
    user_agent: str,
    noc_endpoint: str,
) -> int:
    session = MWClient.make_session()
    session.headers.update({"User-Agent": user_agent})
    noc_client = NOCClient(session, noc_endpoint)

    def mw_client_factory(hostname: str) -> MWClient:
        return MWClient.build(
            session, mw_endpoint, hostname, mediawiki_authorization_locator
        )

    clusters_to_check = build_config(noc_client, mw_client_factory)
    problems = validate_clusters(session, clusters_to_check)
    print(json.dumps(report_problems(problems)))
    return 0


def arg_parser() -> ArgumentParser:
    argparser = ArgumentParser()
    argparser.add_argument(
        "--mw-endpoint", default=DEFAULT_MW_ENDPOINT, help="The MW enpoint to use"
    )
    argparser.add_argument(
        "--mediawiki-authorization-locator",
        default=DEFAULT_AUTH_TOKEN_LOCATOR,
        help="Defines where to source the authorization token needed to make "
        "api requests to private wikis. Contains two parts split by a :. The first "
        "half is a path to a yaml file, the second is a path within the yaml "
        "file delimited by /",
    )
    argparser.add_argument(
        "--user-agent",
        default=DEFAULT_UA,
        help="The user-agent to use when making HTTP requests",
    )
    argparser.add_argument(
        "--noc-endpoint", default=DEFAULT_NOC_ENDPOINT, help="The NOC endpoint to use"
    )
    return argparser


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.exit(main(**dict(vars(arg_parser().parse_args()))))
