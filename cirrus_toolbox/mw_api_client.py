from __future__ import annotations

import logging
import re
from dataclasses import dataclass
from typing import Dict, Iterator, Mapping, Optional

import requests
import yaml
from requests import Response, Session
from requests.adapters import HTTPAdapter
from requests.exceptions import RequestException
from urllib3 import Retry

logger = logging.getLogger(__name__)


DEFAULT_MW_ENDPOINT = "https://mw-api-int.discovery.wmnet:4446"

DEFAULT_AUTH_TOKEN_LOCATOR = (
    "/etc/helmfile-defaults/private/main_services/cirrus-streaming-updater/eqiad.yaml"
    ":app/config_files/app.config.yaml/mediawiki-auth-token"
)


@dataclass
class Page:
    page_id: int
    page_namespace: int
    page_title: str


@dataclass
class CirrusIndexAndReplicaGroup:
    index_name: str
    replica_group: str


class MWApiError(RequestException):
    def __init__(self, response: Response):
        super().__init__(f"API error: {response.json()['error']}")
        self.response = response


class MWClient:
    def __init__(
        self,
        session: Session,
        mw_endpoint: str,
        wiki_hostname: str,
        authorization: Optional[str],
    ):
        self._session = session
        self._api_endpoint = re.sub("/+$", "", mw_endpoint) + "/w/api.php"
        self._wiki_headers = {"host": wiki_hostname}
        if authorization:
            self._wiki_headers["authorization"] = authorization

    @staticmethod
    def make_session(retries: int = 5) -> Session:
        retry_strategy = Retry(
            total=retries, status_forcelist=[404, 429, 500, 502, 503, 504]
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)
        session = requests.session()
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        return session

    @classmethod
    def build(
        cls,
        session: Session,
        mw_endpoint: str,
        wiki_hostname: str,
        auth_locator: Optional[str],
    ) -> MWClient:
        auth = None
        if auth_locator is not None:
            auth = cls._source_auth_header(auth_locator)
        return cls(session, mw_endpoint, wiki_hostname, auth)

    @staticmethod
    def _source_auth_header(locator: str) -> str:
        if ":" not in locator:
            raise ValueError(f"Auth header locator [{locator}] must contain a :")
        file_path, data_path = locator.split(":", 1)
        with open(file_path, "rt") as f:
            data = yaml.safe_load(f)
        for key in data_path.split("/"):
            data = data[key]
        if not isinstance(data, str):
            raise ValueError(
                f"Auth header data path [{data_path}] did not result in a string"
            )
        return f"NetworkSession {data}"

    def _get(self, params: Mapping[str, str]) -> Dict:
        json_params = {"format": "json", "formatversion": "2"}
        params = {**params, **json_params}
        raw_resp = self._session.get(
            self._api_endpoint, params=params, headers=self._wiki_headers
        )
        raw_resp.raise_for_status()
        resp = raw_resp.json()
        if "error" in resp:
            raise MWApiError(raw_resp)
        return resp

    def get_cirrus_index_ns_map(self) -> Dict[int, CirrusIndexAndReplicaGroup]:
        params = {
            "action": "cirrus-config-dump",
            "prop": "replicagroup|namespacemap",
        }
        cirrus_config = self._get(params)
        replica_group = cirrus_config["CirrusSearchConcreteReplicaGroup"]

        return {
            int(ns): CirrusIndexAndReplicaGroup(
                index_name=idx, replica_group=replica_group
            )
            for ns, idx in cirrus_config["CirrusSearchConcreteNamespaceMap"].items()
        }

    def all_pages(
        self,
        ns: Optional[int] = None,
        from_page_title: Optional[str] = None,
        to_page_title: Optional[str] = None,
    ) -> Iterator[Page]:
        continue_params: Optional[Dict[str, str]] = {}
        params = {
            "action": "query",
            "list": "allpages",
            "apfilterredir": "nonredirects",
            "aplimit": "500",
        }
        if ns is not None:
            params["apnamespace"] = str(ns)
        if from_page_title is not None:
            params["apfrom"] = from_page_title
        if to_page_title is not None:
            params["apto"] = to_page_title

        while continue_params is not None:
            logger.info(
                "Fetching all pages from %s",
                continue_params.get("apcontinue", "the begining"),
            )
            body = self._get({**params, **continue_params})
            if "batchcomplete" not in body.keys():
                raise RuntimeError("Invalid response returned")
            for page in body["query"]["allpages"]:
                yield Page(
                    page_id=page["pageid"],
                    page_namespace=page["ns"],
                    page_title=page["title"],
                )
            try:
                continue_params = body["continue"]
            except KeyError:
                continue_params = None

    def expected_indices(self) -> Mapping:
        params = {
            "action": "cirrus-config-dump",
            "prop": "expectedindices",
            "format": "json",
            "formatversion": "2",
        }
        cirrus_config = self._get(params)
        return cirrus_config["CirrusSearchExpectedIndices"]
