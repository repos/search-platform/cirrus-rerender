#!/usr/bin/python3
import itertools
import json
import logging
import sys
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Callable, Optional

import requests
from requests import Session

from cirrus_toolbox.eventgate_sender import EventGateSender
from cirrus_toolbox.mw_api_client import (
    DEFAULT_AUTH_TOKEN_LOCATOR,
    DEFAULT_MW_ENDPOINT,
    MWClient,
    Page,
)
from cirrus_toolbox.noc_client import DEFAULT_NOC_ENDPOINT, NOCClient

logger = logging.getLogger(__name__)

DEFAULT_EVENTGATE_ENDPOINT = "https://eventgate-main.discovery.wmnet:4492/v1/events"
DEFAULT_REASON = "cirrus-force-refresh"
DEFAULT_UA = "cirrus-force-refresh/WMF"
DEFAULT_BATCH_SIZE = 10
DEFAULT_PAGES_PER_SEC = 100


def arg_parser() -> ArgumentParser:
    argparser = ArgumentParser("cirrus-rerender", allow_abbrev=False)
    argparser.add_argument(
        "--mw-endpoint", default=DEFAULT_MW_ENDPOINT, help="The MW enpoint to use"
    )
    argparser.add_argument(
        "--eventgate",
        default=DEFAULT_EVENTGATE_ENDPOINT,
        help="The eventgate endpoint to use",
    )
    argparser.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="number of events to send in one batch",
    )
    argparser.add_argument(
        "--pages-per-sec",
        type=int,
        default=DEFAULT_PAGES_PER_SEC,
        help="Maximum number of pages to emit per second",
    )
    argparser.add_argument(
        "--user-agent",
        default=DEFAULT_UA,
        help="The user-agent to use when making HTTP requests",
    )
    argparser.add_argument(
        "--noc-endpoint", default=DEFAULT_NOC_ENDPOINT, help="The NOC endpoint to use"
    )
    argparser.add_argument(
        "--reason",
        default=DEFAULT_REASON,
        help="Value set in the reason field of the rerender events",
    )
    argparser.add_argument(
        "--mediawiki-authorization-locator",
        default=DEFAULT_AUTH_TOKEN_LOCATOR,
        help="Defines where to source the authorization token needed to make "
        "api requests to private wikis. Contains two parts split by a :. The first "
        "half is a path to a yaml file, the second is a path within the yaml "
        "file delimited by /",
    )
    subparser = argparser.add_subparsers(
        help="Page source", dest="source", required=True
    )
    allpages = subparser.add_parser(
        "allpages", help="Source pages from the allpages list"
    )
    allpages.add_argument("--wiki", required=True)
    allpages.add_argument("--namespace", type=int, help="filter by namespace")
    allpages.add_argument("--from-title", help="extract pages starting from this page")
    allpages.add_argument("--to-title", help="stop extraction at this page")

    ndjson = subparser.add_parser(
        "ndjson",
        help="Source pages from a newline delimited json file. "
        "Each line must contain the fields: wiki, page_id, namespace, title",
    )
    ndjson.add_argument("--path", required=True, type=Path)

    return argparser


def main(args: Namespace, session: Session) -> int:
    session.headers.update({"User-Agent": args.user_agent})
    noc_client = NOCClient(session, args.noc_endpoint)
    eventgate_client = EventGateSender(
        session, args.eventgate, args.batch_size, args.pages_per_sec
    )

    def mw_client_factory(hostname: str) -> MWClient:
        return MWClient.build(
            session,
            mw_endpoint=args.mw_endpoint,
            wiki_hostname=hostname,
            auth_locator=args.mediawiki_authorization_locator,
        )

    if args.source == "allpages":
        hostname = noc_client.get_hostname_by_wiki_id(args.wiki)
        successful = from_allpages(
            mw_client_factory(hostname),
            eventgate_client,
            args.wiki,
            noc_client.get_is_private(args.wiki),
            hostname,
            args.reason,
            args.namespace,
            args.from_title,
            args.to_title,
        )
    elif args.source == "ndjson":
        successful = from_jsonlines(
            noc_client,
            args.path,
            mw_client_factory,
            args.reason,
            eventgate_client,
        )

    return 0 if successful else 1


def from_allpages(
    mw_client: MWClient,
    eventgate_client: EventGateSender,
    wiki_id: str,
    is_private: bool,
    wiki_domain: str,
    reason: str,
    ns: Optional[int],
    from_title: Optional[str],
    to_title: Optional[str],
) -> bool:
    pages = mw_client.all_pages(ns, from_title, to_title)
    ns_map = mw_client.get_cirrus_index_ns_map()
    eventgate_client.send_page_rerenders(
        wiki_domain, wiki_id, is_private, reason, pages, ns_map
    )
    return True


def from_jsonlines(
    noc_client: NOCClient,
    path: Path,
    mw_client_factory: Callable[[str], MWClient],
    reason: str,
    eventgate_client: EventGateSender,
) -> bool:
    with path.open("rt") as f:
        rows = [json.loads(line) for line in f]
    rows.sort(key=lambda row: row["wiki"])

    rerender_args = []
    error = False
    for wiki_id, grouped in itertools.groupby(rows, key=lambda row: row["wiki"]):
        pages = [
            Page(row["page_id"], row["namespace"], row["title"]) for row in grouped
        ]
        hostname = noc_client.get_hostname_by_wiki_id(wiki_id)
        mw_client = mw_client_factory(hostname)
        try:
            ns_map = mw_client.get_cirrus_index_ns_map()
        except requests.exceptions.RequestException:
            logger.exception(
                f"Failed to source nsmap for [{wiki_id}] from [{hostname}]."
            )
            error = True
            continue
        # Collect all the calls into an array so we can emit all the errors up
        # front instead of emitting errors 20 minutes into a run when we
        # happen to get to a wiki we can't talk to, or failing after the first
        # error instead reporting of all of them.
        is_private = noc_client.get_is_private(wiki_id)
        rerender_args.append((hostname, wiki_id, is_private, reason, pages, ns_map))

    if error:
        logger.error(f"Error initializing from [{path}]. Cannot issue rerenders")
        return False

    for args in rerender_args:
        eventgate_client.send_page_rerenders(*args)

    return True


def entrypoint() -> None:
    logging.basicConfig(level=logging.INFO)
    args = arg_parser().parse_args()
    sys.exit(main(args, MWClient.make_session()))


if __name__ == "__main__":
    entrypoint()
