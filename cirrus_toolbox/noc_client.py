import urllib.parse
from functools import lru_cache
from typing import Any, Mapping

from requests import Session

DEFAULT_NOC_ENDPOINT = "https://noc.wikimedia.org/wiki.php"


class NOCClient:
    def __init__(
        self, session: Session, noc_endpoint: str = "https://noc.wikimedia.org/wiki.php"
    ):
        self._session = session
        self._noc_endpoint = noc_endpoint

    @lru_cache
    def _for_wiki(self, wiki_id: str) -> Mapping[str, Any]:
        resp = self._session.get(
            self._noc_endpoint, params={"wiki": wiki_id, "format": "json"}
        )
        resp.raise_for_status()
        return resp.json()

    def get_hostname_by_wiki_id(self, wiki_id: str) -> str:
        endpoint = self._for_wiki(wiki_id)["wgCanonicalServer"]
        return str(urllib.parse.urlparse(endpoint).hostname)

    def get_is_private(self, wiki_id: str) -> bool:
        try:
            return bool(self._for_wiki(wiki_id)["wmgPrivateWiki"])
        except KeyError:
            raise RuntimeError(f"Available map: {self._for_wiki(wiki_id)}")
