import logging
import re
import time
import uuid
from datetime import datetime, timezone
from typing import Any, Callable, Dict, Iterable, List, TypeVar

from requests import Session

from cirrus_toolbox.mw_api_client import CirrusIndexAndReplicaGroup, Page

logger = logging.getLogger(__name__)


PAGE_RERENDER_STREAM = {
    "public": "mediawiki.cirrussearch.page_rerender.v1",
    "private": "mediawiki.cirrussearch.page_rerender.private.v1",
}

T = TypeVar("T")


def random_id() -> str:
    return str(uuid.uuid4())


def ratelimit(seq: Iterable[T], sec_per_item: float) -> Iterable[T]:
    # We could start previous at 0.0, but the tests are more obviously correct
    # if we pause for all items without skipping the first.
    previous = time.monotonic()
    for item in seq:
        current = time.monotonic()
        elapsed = current - previous
        previous = current

        delay = sec_per_item - elapsed
        if delay > 0:
            time.sleep(delay)
            previous += delay
        yield item


class EventGateSender:
    def __init__(
        self,
        session: Session,
        eventgate_endpoint: str,
        batch_size: int,
        pages_per_sec: int = 100,
        clock: Callable[[], datetime] = datetime.now,
        id_gen: Callable[[], str] = random_id,
    ):
        self._eventgate_endpoint = eventgate_endpoint
        self._session = session
        self._batch_size = batch_size
        self._pages_per_sec = pages_per_sec
        self._clock = clock
        self._id_gen = id_gen

    def _batch(self, pages: Iterable[T]) -> Iterable[List[T]]:
        pages_batch = []
        for p in pages:
            pages_batch.append(p)
            if len(pages_batch) >= self._batch_size:
                yield pages_batch
                pages_batch = []
        if len(pages_batch) > 0:
            yield pages_batch

    def _meta_field(self, domain: str, stream: str) -> Dict[str, str]:
        return {
            "id": self._id_gen(),
            "domain": domain,
            "stream": stream,
            "request_id": self._id_gen(),
        }

    def _to_page_rerender(
        self,
        domain: str,
        wiki_id: str,
        is_private: bool,
        page: Page,
        reason: str,
        ns_map: Dict[int, CirrusIndexAndReplicaGroup],
    ) -> Dict[str, object]:
        index_and_replica = ns_map[page.page_namespace]
        dt_iso8601 = (
            self._clock().replace(microsecond=0, tzinfo=timezone.utc).isoformat()
        )
        dt_iso8601 = re.sub(r"\+00:00$", "Z", dt_iso8601)
        stream_type = "private" if is_private else "public"
        return {
            "$schema": "/mediawiki/cirrussearch/page_rerender/1.0.0",
            "meta": self._meta_field(
                domain=domain, stream=PAGE_RERENDER_STREAM[stream_type]
            ),
            "dt": dt_iso8601,
            "cirrussearch_cluster_group": index_and_replica.replica_group,
            "cirrussearch_index_name": index_and_replica.index_name,
            "page_id": page.page_id,
            "page_title": page.page_title,
            "namespace_id": page.page_namespace,
            "wiki_id": wiki_id,
            "is_redirect": False,
            "reason": reason,
        }

    def send_page_rerenders(
        self,
        domain: str,
        wiki_id: str,
        is_private: bool,
        reason: str,
        pages: Iterable[Page],
        ns_map: Dict[int, CirrusIndexAndReplicaGroup],
    ) -> None:
        self.send(
            map(
                lambda p: self._to_page_rerender(
                    domain, wiki_id, is_private, p, reason, ns_map
                ),
                pages,
            )
        )

    def send(self, events: Iterable[Dict[Any, Any]]) -> None:
        sec_per_batch = self._batch_size / self._pages_per_sec
        for batch in ratelimit(self._batch(events), sec_per_batch):
            logger.debug(
                "Sending %d events to %s", len(batch), self._eventgate_endpoint
            )
            resp = self._session.post(self._eventgate_endpoint, json=batch)
            resp.raise_for_status()
